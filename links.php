<?php
if (is_page() && $post->post_parent) :
  $request_data = array('child_of'    => $post->post_parent,
                        'parent'      => $post->post_parent,
                        'sort_column' => 'post_title',
                        'sort_order'  => 'asc'
                        );
  $pagelist    = get_pages(http_build_query($request_data));
  $pages       = array();
  foreach ($pagelist as $page) {
     $pages[] += $page->ID;
  }
  $pages_count = count($pages);
  $current     = array_search($post->ID, $pages);

  $prevID_A_1  = isset($pages[$current-2]) ? $pages[$current-2] : null;
  $prevID_A_2  = isset($pages[$current-1]) ? $pages[$current-1] : null;

  $prevID_B_1  = end($pages);
  $prevID_B_2  = array_slice($pages, -2);
  $prevID_B_3  = isset($prevID_B_2[0]) ? $prevID_B_2[0] : null;
  $prevID_B_4  = isset($prevID_B_2[1]) ? $prevID_B_2[1] : null;

  $nextID_C_1  = isset($pages[$current+2]) ? $pages[$current+2] : null;
  $nextID_C_2  = isset($pages[$current+1]) ? $pages[$current+1] : null;

  $nextID_D_1  = isset($pages[0]) ? $pages[0] : null;
  $nextID_D_2  = isset($pages[1]) ? $pages[1] : null;

  $classes     = 'col-sm-6 col-md-3 page-nav-link';

  $default_image = get_avatar_url(0, array(
    'size'          => 320,
    'default'       => 'mm',
    'force_default' => true
  )); ?>

  <div class="container">
    <div class="row page-navigation">
      <?php if (!empty($prevID_A_1)) : ?>
        <div class="<?php echo $classes; ?>">
          <a href="<?php echo esc_url(get_permalink($prevID_A_1)); ?>" title="<?php echo esc_attr(get_the_title($prevID_A_1)); ?>"><img src="<?php echo (get_the_post_thumbnail_url($prevID_A_1) ? get_the_post_thumbnail_url($prevID_A_1, 'medium') : $default_image); ?>"><?php echo esc_attr(get_the_title($prevID_A_1)); ?></a>
        </div>

        <div class="<?php echo $classes; ?>">
          <a href="<?php echo esc_url(get_permalink($prevID_A_2)); ?>" title="<?php echo esc_attr(get_the_title($prevID_A_2)); ?>"><img src="<?php echo (get_the_post_thumbnail_url($prevID_A_2) ? get_the_post_thumbnail_url($prevID_A_2, 'medium') : $default_image); ?>"><?php echo esc_attr(get_the_title($prevID_A_2)); ?></a>
        </div>
      <?php elseif (empty($prevID_A_1) && !empty($prevID_A_2)) : ?>
        <?php if ($pages_count > 4) : ?>
          <div class="<?php echo $classes; ?>">
            <a href="<?php echo esc_url(get_permalink($prevID_B_1)); ?>" title="<?php echo esc_attr(get_the_title($prevID_B_1)); ?>"><img src="<?php echo (get_the_post_thumbnail_url($prevID_B_1) ? get_the_post_thumbnail_url($prevID_B_1, 'medium') : $default_image); ?>"><?php echo esc_attr(get_the_title($prevID_B_1)); ?></a>
          </div>
        <?php endif; ?>

        <div class="<?php echo $classes; ?>">
          <a href="<?php echo esc_url(get_permalink($prevID_A_2)); ?>" title="<?php echo esc_attr(get_the_title($prevID_A_2)); ?>"><img src="<?php echo (get_the_post_thumbnail_url($prevID_A_2) ? get_the_post_thumbnail_url($prevID_A_2, 'medium') : $default_image); ?>"><?php echo esc_attr(get_the_title($prevID_A_2)); ?></a>
        </div>
      <?php elseif (empty($prevID_A_1) && empty($prevID_A_2)) : ?>
        <?php if ($pages_count > 4) : ?>
          <div class="<?php echo $classes; ?>">
            <a href="<?php echo esc_url(get_permalink($prevID_B_3)); ?>" title="<?php echo esc_attr(get_the_title($prevID_B_3)); ?>"><img src="<?php echo (get_the_post_thumbnail_url($prevID_B_3) ? get_the_post_thumbnail_url($prevID_B_3, 'medium') : $default_image); ?>"><?php echo esc_attr(get_the_title($prevID_B_3)); ?></a>
          </div>
        <?php endif; ?>

        <?php if ($pages_count > 3) : ?>
          <div class="<?php echo $classes; ?>">
            <a href="<?php echo esc_url(get_permalink($prevID_B_4)); ?>" title="<?php echo esc_attr(get_the_title($prevID_B_4)); ?>"><img src="<?php echo (get_the_post_thumbnail_url($prevID_B_4) ? get_the_post_thumbnail_url($prevID_B_4, 'medium') : $default_image); ?>"><?php echo esc_attr(get_the_title($prevID_B_4)); ?></a>
          </div>
        <?php endif; ?>
      <?php endif; ?>


      <?php if (!empty($nextID_C_1)) : ?>
        <div class="<?php echo $classes; ?>">
          <a href="<?php echo esc_url(get_permalink($nextID_C_2)); ?>" title="<?php echo esc_attr(get_the_title($nextID_C_2)); ?>"><img src="<?php echo (get_the_post_thumbnail_url($nextID_C_2) ? get_the_post_thumbnail_url($nextID_C_2, 'medium') : $default_image); ?>"><?php echo esc_attr(get_the_title($nextID_C_2)); ?></a>
        </div>

        <div class="<?php echo $classes; ?>">
          <a href="<?php echo esc_url(get_permalink($nextID_C_1)); ?>" title="<?php echo esc_attr(get_the_title($nextID_C_1)); ?>"><img src="<?php echo (get_the_post_thumbnail_url($nextID_C_1) ? get_the_post_thumbnail_url($nextID_C_1, 'medium') : $default_image); ?>"><?php echo esc_attr(get_the_title($nextID_C_1)); ?></a>
        </div>
      <?php elseif (empty($nextID_C_1) && !empty($nextID_C_2)) : ?>
        <div class="<?php echo $classes; ?>">
          <a href="<?php echo esc_url(get_permalink($nextID_C_2)); ?>" title="<?php echo esc_attr(get_the_title($nextID_C_2)); ?>"><img src="<?php echo (get_the_post_thumbnail_url($nextID_C_2) ? get_the_post_thumbnail_url($nextID_C_2, 'medium') : $default_image); ?>"><?php echo esc_attr(get_the_title($nextID_C_2)); ?></a>
        </div>

        <?php if ($pages_count > 4) : ?>
          <div class="<?php echo $classes; ?>">
            <a href="<?php echo esc_url(get_permalink($nextID_D_1)); ?>" title="<?php echo esc_attr(get_the_title($nextID_D_1)); ?>"><img src="<?php echo (get_the_post_thumbnail_url($nextID_D_1) ? get_the_post_thumbnail_url($nextID_D_1, 'medium') : $default_image); ?>"><?php echo esc_attr(get_the_title($nextID_D_1)); ?></a>
          </div>
        <?php endif; ?>
      <?php elseif (empty($nextID_C_1) && empty($nextID_C_2)) : ?>
        <?php if ($pages_count > 3) : ?>
          <div class="<?php echo $classes; ?>">
            <a href="<?php echo esc_url(get_permalink($nextID_D_1)); ?>" title="<?php echo esc_attr(get_the_title($nextID_D_1)); ?>"><img src="<?php echo (get_the_post_thumbnail_url($nextID_D_1) ? get_the_post_thumbnail_url($nextID_D_1, 'medium') : $default_image); ?>"><?php echo esc_attr(get_the_title($nextID_D_1)); ?></a>
          </div>
        <?php endif; ?>

        <?php if ($pages_count > 4) : ?>
          <div class="<?php echo $classes; ?>">
            <a href="<?php echo esc_url(get_permalink($nextID_D_2)); ?>" title="<?php echo esc_attr(get_the_title($nextID_D_2)); ?>"><img src="<?php echo (get_the_post_thumbnail_url($nextID_D_2) ? get_the_post_thumbnail_url($nextID_D_2, 'medium') : $default_image); ?>"><?php echo esc_attr(get_the_title($nextID_D_2)); ?></a>
          </div>
        <?php endif; ?>
      <?php endif; ?>
    </div>
  </div>
<?php endif; ?>